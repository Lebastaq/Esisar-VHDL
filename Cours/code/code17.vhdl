architecture fsmd of generator is
  type StateType is (Idle, Calc);
  
  signal next_state, present_state: StateType;

  -- pour chaque registre différé on déclare deux signaux
  signal cpt_reg, cpt_next: unsigned (2 downto 0);

begin
  process(clk, rst)
  begin
    if clk'event and clk='1' then
      if rst='1' then
        present_state <= Idle;
        cpt_reg <= (others=>'0');
      else
        present_state <= next_state;
        cpt_reg <= cpt_next;
      end if;
    end if;
  end process;

  process(present_state, go, cpt_reg, stop)
    begin
      pulse <= '0';
      cpt_next <=(others=>'0');
      
      case present_state is
        when Idle=>
          if go='1' then
            next_state<=Calc;
          else
            next_state<=Idle;
          end if;
        when Calc=>
          pulse<='1';
          cpt_next<=cpt_reg + 1; -- on affect cpt_next et pas cpt_reg !!

          if(cpt_reg=4 or stop='1') then
            next_state<=Idle;
          else
            next_state<=Calc;
          end if;
      end case;
    end process;
    
end architecture;
