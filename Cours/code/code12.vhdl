-- Affectation

-- compliqué et possible de se tromper
out1<=reg(0) and not(reg(1)) and reg(2);

-- mieux
out1 <= '1' when reg="101" else 0;


-- Décalage
-- solution pas très efficace - utilisation de 3 bascules
reg(0) <= in1;
reg(1) <= reg(0);
reg(2) <= reg(1);

-- solution plus efficace
-- [ in1; reg(0); reg(1) ]
reg <= in1 & reg(0 to 1);
