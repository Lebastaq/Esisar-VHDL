library IEEE;
use IEEE.std_logic_1164.all;

entity chrono is
  port( clk, rst, en: in std_logic;
        min: out std_logic_vector(5 downto 0);
        sec: out std_logic_vector(5 downto 0));
end entity;

architecture struct of chrono is
  signal sec_en: std_logic;
  signal min_en: std_logic;
  signal min_en1: std_logic;
begin
  count1M: entity work.countN(rtl)
    generic map(1000000, 20) -- compter juqu'a un million - 20 bits
    port map(clk, -- connecter l'entrée clock de countN au signal clk du port
             en, -- de meme
             rst, -- de meme
             OPEN, -- on laisse le port "count" dans le vide
             sec_en -- connecter l'entrée count_en de countN au signal interne
             );

  count60_sec: entity work.countN(rtl)
    generic map(60, 6)
    port map(clk,
             sec_en,
             rst,
             sec,
             min_en1);
  
  min_en <= min_en1 and sec_en; -- comme dit précédemment, on veut compter
  -- uniquement quand min_en ET sec_en sont activés

  count60_min: entity work.countN(rtl)
    generic map(60, 6)
    port map(clk, min_en, rst, min, OPEN);

end architecture;
