process(clk, rst) -- process sensible au rst
begin
  if(rst='1') or (q=10) then
    q<=0;
  else if (clk'event) and (clk='1')
    q<=q+1;
  end if;
end process;
