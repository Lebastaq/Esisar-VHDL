library IEEE;
use IEEE.std_logic_1164.all;

entity tb_detect is
end entity;

architecture bhv of tb_detect is
  signal rst, in1, out1: std_logic;
  signal clk: std_logic:='0';

begin
  i1: entity work_detect(rtl)
    port map(clk, in1, rst, out1);

  -- période 10ns
  clk <= (not clk) after 5ns;
  rst <= '0', '1' after 10ns, '0' after 20ns;
  in1 <= '0', '1' after 20ns, '0' after 30ns, '1' after 40ns, '0' after 50ns, '1' after 70ns;
end architecture;
