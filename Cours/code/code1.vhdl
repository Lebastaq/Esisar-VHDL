library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all; -- pour les unsigned

-- entite compteur generique
entity countN is
  generic(n: integer, k; integer);
  -- n: modulo
  -- k: nombre de bits sur la sortie
  
  port( clk, en, rst: in std_logic;
        count: out std_logic_vector(k-1 downto 0);
        count_en: out std_logic );
end entity;


-- architecture "rtl"
architecture rtl of countN is

  -- on ne peut pas se servir de count car c'est un signal interne
  -- attention quand on commence q ne vaut pas 0, donc obligé de faire un reset
  --     avant de commencer a compter
  signal q: unsigned(k-1 downto 0)

  begin
    process( clk ) -- reset synchrone
      if clk'event and clk='1' then
        if rst='1' or q=n-1 then -- modulo ou reset
          q <= (others=>'0'); -- remettre a 0 tous les bits du tableau
          -- attention: ne pas mettre l'assignation de count ici, car l'horloge
          -- exécute une instruction par coup d'horloge.
          -- L'assignation de count sera donc décalée par rapport à q !!
        elseif en='1' then
          q <= q+1;
        end if;
      end if;
    end process;

  -- fonction de conversion indiquée dans la doc
  count<= std_logic_vector(q);
  count_en <= '1' when q=n-1 else '0'

end architecture;
