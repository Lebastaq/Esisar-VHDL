signal a:unsigned(2 downto 0)
-- reg(2) reg(1) reg(0)
-- utile pour représenter des nombres entiers
-- pour des opérations arithmétiques

signal b:std_logic_vector(0 to 2)
-- pour des opérations logiques
