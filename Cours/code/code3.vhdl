entity synchronizer is
  port( in_async, clk: in bit;
        out_sync: out bit);
end entity;

architecture rtl of synchronizer is

  signal q1: bit;
    
  begin
    process (clk)
      if clk'event and clk='1' then
        -- code exécuté en "sortant", donc a la fin q1 = in_async[ancien]
        -- et out_sync = q1[ancien]
        -- implique un retard de deux périodes de clk
        q1 <= in_async; -- stockage dans 1e bascule
        out_sync <= q1; -- stockage dans 2e bascule
      end if;
    end process;
    
end architecture;
