-- déclaration des ports d’entrées/sorties de la FSM
entity generateur is port (
  go, stop, clk : in bit;
  pulse : out bit);
end generateur;



architecture state_machine of generateur is

-- Déclaration d’un type énuméré composé des noms des états
type StateType is (idle, delay1, delay2, delay3, delay4, delay5);
-- Déclaration de deux signaux du type précédent
-- Valeur par défaut: idle
signal present_state, next_state : StateType;


-- Premier process qui définir les sorties et
-- états suivants accessibles en fonction des entrées
state_comb:process(present_state, go, stop)
begin
  case present_state is
    when idle => pulse <= ‘0’;
       if go = '1' then
         next_state <= delay1;
       else -- /!\ Ne pas oublier le else !
         next_state <= idle;
       end if;
    when delay1 => pulse <= ‘1’';
       if stop = '1' then
         next_state <= idle ;
       else
         next_state <= delay2;
       end if; -- etc ...


-- Second process qui décrit le passage d'un état à l’autre
-- sur les fronts montants de l’horloge
state_clocked:process(clk)
begin
  if (clk'event and clk='1') then
    present_state <= next_state;
  end if;
end process state_clocked;

end architecture state_machine;
