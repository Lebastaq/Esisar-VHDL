library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity multiplieur is
  port(clk, start, reset: in std_logic;
       ready: out std_logic;
       a_in, b_in: in std_logic_vector(9 downto 0);
       -- 10 bits * 10 bits = 20 bits
       r_out: out std_logic_vector(19 downto 0)
       );
end entity;


architecture fmsd of multiplieur is
  type StateType is (Idle, Init, Calc);
  signal next_state, present_state: StateType;

  -- pas besoin car jamais affecté
  -- signal a_in_reg, a_in_next, b_in_reg, b_in_next: unsigned (2 downto 0);
  
  signal r_out_next, r_out_reg, r_reg, r_next: unsigned (19 downto 0);
  signal a_reg, a_next, n_reg, n_next: unsigned (9 downto 0);

begin
  -- reset déjà dans les signaux internes
  process(clk)
  begin
    if clk'event and clk='1' then
      if rst='1' then
        present_state <= Idle;
        r_out_reg <= (others=>'0');
        r_reg     <= (others=>'0');
        a_reg     <= (others=>'0');
        n_reg     <= (others=>'0');
      else
        present_state <= next_state;
        r_out_reg <= r_out_next;
        r_reg     <= r_next;
        a_reg     <= a_next;
        b_reg     <= b_next;
      end if;
    end if;
end process;
