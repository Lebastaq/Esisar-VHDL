process(clk) -- process non sensible au reset
begin
  if(clk'event and clk='1') then
    if(rst='1') or (q=9) then
      q<=0;
    else
      q<=q+1;
    end if;
  end if;
end process;
