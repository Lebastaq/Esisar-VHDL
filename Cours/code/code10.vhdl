library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity detect is
  port(clk, in1, rst: in std_logic;
       out1: out std_logic);
end entity;

architecture rtl of detect is

  signal reg: std_logic_vector(0 to 2);

  -- car on n'a pas le droit de lire dans une variable de sortie!
  signal temp_out_1: std_logic;

begin
  out1 <= temp1;
  temp_out_1 <= '1' when reg="101" else 0;
  
  process( clk )
    begin
      if (clk='1' and clk'event) then
        if rst='1' then 
          reg <= (others=>'0'); -- req <= "000"
        elsif temp_out_1='1'
          reg <= in1 & "00"
        else
          reg <= in1 & reg(0 to 1);
        end if;
      end if;
    end process;
end architecture;
