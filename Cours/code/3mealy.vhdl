architecture fsm_mealy of detector is
  

  type StateType is (E0, E1, E2, E3);
  -- valeur par défaut: E0
signal cur_s, nex_s: StateType;

-- on part du principe que in1 est synchrone
-- donc pas de problemes de clk
state_comb: process(cur_s, in1)
begin
  out1<=0; -- valeur par défaut
  case cur_s is
    when E0=>
      if in1='1' then
        nex_s<=E1;
      else
        nex_s<=E0;
      end if;
    when E1=>
      -- ...
    when E2=>
      if in1='1' then
        out1<=1
        nex_s<=E0;
      else
        nex_s<=E0;
      end if;
  end case;
end process;


seq: process(clk)
begin
  if (clk'event and clk='1') then
    if rst='1' then
      cur_s<=E0;
    else
      cur_s<=nex_s;
    end if;
  end if;
end process;
