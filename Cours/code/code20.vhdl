process(clk, reset, x, tap)
begin
  if reset='1' then -- clear shift register
    for k in 0 to 3 loop
      tap(K) <= 0;
    end loop;
    y <= 0;

  elsif clk'event and clk='1' then
    y <= 2 * tap(1) + tap(1) + tap(1) / 2 + tap(1) / 4 + 2 * tap(2) + tap(2) + tap(2) / 2 + tap(2) / 4 - tap(3) - tap(0);
    for i in 3 downto 1 loop
      tap(i) <= tap(i-1); -- décalage
    end loop;
  end if;
  tap(0) <= x; -- input
end process;

    
  
