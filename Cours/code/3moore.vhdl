architecture fsm_moore of detector is

type StateType is (E0, E1, E2, E3);
signal cur_s, nex_s: StateType;

-- sensible à l'état présent car la sortie dépend de
-- l'entrée ET de l'état
state_comb: process(cur_s, in1)
begin
  out1<=0; -- valeur par défaut
  case cur_s is
    when E0=>
      if in1='1' then
        nex_s<=E1;
      else
        nex_s<=E0;
      end if;
    when E1=>
      -- ...
    when E2=>
      -- ...
    when E3=>
      out1<=1;
      if in1='1' then
        nex_s<=E1;
      else
        nex_s<=E2;
      end if;
  end case;
end process;


seq: process(clk)
begin
  if (clk'event and clk='1') then
    if rst='1' then
      cur_s<=E0;
    else
      cur_s<=nex_s;
    end if;
  end if;
end process;
