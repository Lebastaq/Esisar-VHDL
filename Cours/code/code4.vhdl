library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pulse_generator is
  port( clk, go, stop: in std_logic;
        pulse :out std_logic);
end entity;

architecture rtl of pulse_generator is

  signal q: unsigned(2 downto 0);
  
begin
  process(clk)
  begin
    if clk='1' and clk'event then
      if stop='1' or q=5 then
        pulse <= '0';
        q<=(others=>'0');
      elsif go='1' or pulse='1' then
        pulse <= '1';
        q<=q+1;
      end if;
    end if;
  end if;
end process;
end architecture;
