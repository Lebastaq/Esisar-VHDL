(TeX-add-style-hook
 "Cours"
 (lambda ()
   (TeX-run-style-hooks
    "preample"
    "chap1"
    "chap2"
    "chap3"
    "chap4"
    "amend1"
    "tikz"
    "lstlinebgrd"
    "listingsutf8"
    "color")))

