library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.all;

entity FM0_Encoder is
    Port ( enable				: in  STD_LOGIC;
           clk					: in  STD_LOGIC;
           data_in				: in  STD_LOGIC;
           violation 			: in  STD_LOGIC;
		   next_data_enable  	: out  STD_LOGIC;
           FM0output 			: out  STD_LOGIC);
end FM0_Encoder;

architecture Behavioral of FM0_Encoder is

type typeState is (S1_a, S1_b, S2_a, S2_b, S3_a, S3_b, S4_a, S4_b);
-- next state, present state
signal n_state, p_state: typeState;
signal n_sortie, p_sortie: std_logic;

-- activ� une p�riode de clock sur deux pour lire la sortie
signal enable_sortie: std_logic;

begin
    
    -- activation de la lecture de la sortie
    clk_output_mngr: process(clk)
    begin
      if enable_sortie = '1' then
        enable_sortie <= '0';
      else
        enable_sortie <= '1';
      end if;
    end process;
    
    -- changements d'�tats selon la clock
    state_manager: process(clk)
    begin
        if clk'event and clk='1' then
          if enable='1' then
             p_state <= n_state;
             p_sortie <= n_sortie;
          else
             -- reset
             p_state <= S1_a;
             p_sortie <= '0';
          end if;
         end if;
    end process;
    
    -- assignation des sorties en asynchrone
    sorties: process(p_state)
      begin
        case p_state is
          when S1_a =>
            n_sortie <= '1';
          when S1_b =>
            n_sortie <= '1';
          when S4_a =>
            n_sortie <= '0';
          when S4_b =>
            n_sortie <= '0';
          when S2_a =>
            n_sortie <= '1';
          when S2_b =>
            n_sortie <= '0';
          when S3_a =>
            n_sortie <= '0';
          when S3_b =>
            n_sortie <= '1';
         end case;
    end process;
    
    -- logique de transitions
    changement_etats: process(clk, p_state)
      begin
        case p_state is
        
            -- S1
            when S1_a =>
              n_state <= S1_b;
            when S1_b =>
              if data_in='1' then
                n_state <= S4_a;
              else
                n_state <= S3_a;
              end if;
              
            -- S4
            when S4_a =>
              n_state <= S4_b;
            when S4_b =>
              if data_in='1' then
                n_state <= S1_a;
              else
                n_state <= S2_a;
              end if;
              
            -- S2
            when S2_a =>
              n_state <= S2_b;
            when S2_b =>
              if data_in='1' then
                n_state <= S1_a;
              else
                n_state <= S2_a;
              end if;
              
            -- S3
            when S3_a =>
               n_state <= S3_b;
            when S3_b =>
              if data_in='1' then
                n_state <= S4_a;
              else
                n_state <= S3_a;
              end if;
         end case;
             
      end process;
        
     -- on active quand on a pass� deux signaux de clock (SX_a + Sx_b)
     next_data_enable <= '1' when enable='1' else '0';

     -- garder la m�me sortie si violation
     FM0output <= not n_sortie when violation = '0' else not p_sortie;
        
end Behavioral;

