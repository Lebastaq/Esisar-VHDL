LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

entity tb_Miller_Encoder is
end tb_Miller_Encoder;
 
ARCHITECTURE behavior OF tb_Miller_Encoder IS 

    -- Re-déclaration du composant
    COMPONENT Miller_Encoder
     Port ( enable		    : in  STD_LOGIC;
              clk                   : in  STD_LOGIC;
              BLF                   : in  STD_LOGIC;
              end_encode            : in  STD_LOGIC;
              data_in               : in  STD_LOGIC;
              M                     : in  STD_LOGIC_VECTOR(1 downto 0);
              next_data_enable      : out  STD_LOGIC;
              Miller_output         : out  STD_LOGIC);
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal enable : std_logic := '0';
   signal end_encode : std_logic := '0';
   signal data_in : std_logic := '0';
   signal BLF : std_logic := '0';
   -- on ne peut pas mettre directement de valeur pour les vecteurs
   signal M : std_logic_vector(1 downto 0);
       
 	--Outputs   
   signal next_data_enable : std_logic;
   signal Miller_output : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
   -- on met M à 4
   M <= "10";
 
   uut: Miller_Encoder
   PORT MAP (
          clk => clk,
          enable => enable,
          end_encode => end_encode,
          data_in => data_in,
          BLF => BLF,
          M => M,
          next_data_enable => next_data_enable,
          Miller_output => Miller_output
        );

   -- Génération signal clock
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		
		clk <= '0';
		wait for clk_period/2;
   end process;

   -- génération signal enable pour démarrer l'encodage
   en_process: process
      begin
           enable <= '0';
           wait for clk_period*14;
           
           enable <= '1';
           wait for clk_period;
           
    
           wait for clk_period*40;    
      end process;

   -- génération signal blf
   -- doit être actif 1/2 période apres le signal enable
   blf_process: process
   begin
        BLF <= '0';
        wait for clk_period*14;
        
        BLF <= '1';
        wait for clk_period;
        
        BLF <= '0';    
        wait for clk_period*40;    
   end process;

   -- signal end_encode actif une demi période d horloge juste
   -- avant le signal enable
   ee_process: process
      begin
         end_encode <= '0';
         wait for clk_period*12;
         wait for clk_period;
           
         end_encode <= '1';
         wait for clk_period;
           
         end_encode <= '0';
         wait for clk_period*40;    
      end process;

   -- signal fourni en entree
   -- cf chronogramme
   di_process :process
   begin        
        data_in <= '0';   
                
        -- sur échantillonage *2 à cause du sur échantillonage          
        wait for clk_period*30;
        
        data_in <= '1';        
        wait for clk_period*8;
		
	data_in <= '0';
		
	wait for clk_period*8;
        wait for clk_period*8;
		
        data_in <= '1';
                
        wait for clk_period*8;
        wait for clk_period*8;
		
        data_in <= '0';
        wait;
   end process;

END;
