LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

entity tb_FM0_Encoder is
end tb_FM0_Encoder;
 
ARCHITECTURE behavior OF tb_FM0_Encoder IS 
 
    COMPONENT FM0_Encoder
     Port ( enable				: in  STD_LOGIC;
               clk              : in  STD_LOGIC;
               data_in          : in  STD_LOGIC;
               violation        : in  STD_LOGIC;
               next_data_enable : out  STD_LOGIC;
               FM0output        : out  STD_LOGIC);
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal enable : std_logic := '0';
   signal violation : std_logic := '0';
   signal data_in : std_logic := '0';
       
 	--Outputs   
   signal next_data_enable : std_logic;
   signal FM0output : std_logic;

   -- Clock period
   constant clk_period : time := 10 ns;
 
BEGIN
   uut: FM0_Encoder
   PORT MAP (
          clk => clk,
          enable => enable,
          violation => violation,
          data_in => data_in,
          next_data_enable => next_data_enable,
          FM0output => FM0output
        );

   -- Clock process
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		
		clk <= '0';
		wait for clk_period/2;
   end process;
   
   -- enable 
   en_process: process
      begin
           enable <= '0';
           -- par s�curit�
           wait for clk_period*10;
           
           enable <= '1';
           wait for clk_period;
           
    
           wait for clk_period*80;    
      end process;
   
   di_process :process
   begin        
        data_in <= '0';   
                        
        wait for clk_period*15;
        
        data_in <= '1';    
        violation <= '1'   ;
        
        wait for clk_period*1;
        
        violation <= '0'  ;
        wait for clk_period*2;
           
        wait for clk_period*4;
		
        data_in <= '0';
		
        wait for clk_period*6;
		
        data_in <= '1';
                
        wait for clk_period*2;
		
        data_in <= '0';
        wait;
   end process;

END;
