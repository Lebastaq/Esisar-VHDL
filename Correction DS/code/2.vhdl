architecture fsm_moore of toggle is
  type StateType is(E0, E1);
  signal present_state, next_state: StateType;

begin
  process(clk, reset)
  begin
    if reset='1' then
      present_state <= E0;
    elsif clk'event and clk='1' then
      present_state <= next_state;
    end if;
  end process;

  process()
  begin
    case present_state is
      when E0 =>
        q<= '0';
        if t='1' then
          next_state <= E1;
        else
          next_state <= E0;
        end if;
      when E1 =>
        q<= '1';
        if t='1' then
          next_state <= E0;
        else
          next_state <= E1;
        end if;
    end case;
  end process;
  end architecture;
