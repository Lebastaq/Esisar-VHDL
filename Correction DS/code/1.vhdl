library IEEE;
use IEEE.std_logic_1164.all;
-- on suppose ceci aussi présent dans la suite

entity Toggle is
  port(clk, t, reset: in std_logic;
       q:             out std_logic);
end entity;

architecture regular of Toggle is
  signal q_temp: std_logic;
  -- si on met une valeur dans q_temp: pas synthétisable
begin

  q<=q_temp;
  
  process(clk, reset)
  begin
    if reset='1' then
      q_temp<='0';
    elsif (clk'event and clk='1') then
      if t='1' then
        q_temp <= not(q_temp);
      end if;
    end if;
  end process;
end architecture;

  
