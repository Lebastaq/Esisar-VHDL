architecture behav of cell is
  signal counter: unsigned(3 downto 0);
  signal enable: std_logic;

begin
  proc1: process(rst, clk)
  begin
    if rst='1' then
      -- on commence à 
      counter<=(others=>'1');
    end if;
  elsif(clk'event and clk='1') then
    if enable = '1' then
      counter<= counter +1;
      -- else: mémorisation implicite
    end if;
  end if;
end process;

proc3: process(clk, rst, enable)
begin
  if rst='1' then
    pulse <= '0'
  elsif clk'event and clk='1' then
      pulse <= enable;
  end if;
end process;

enable <='1' when start='1'
    or counter(3 downto 0) < '8' else 0;

end architecture;
