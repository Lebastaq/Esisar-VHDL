entity moore is port(
  rst: in std_logic;
  clk: in std_logic;
  s: in std_logic;
  sds: out std_logic;
  sfs: out std_logic;
  );
end moore;

architecture arch_moore of moore is
  type states is (s0, s1, s2, s3);
  signal cur_state, nex_state: states;
  
begin

  -- 1er process:  horloge
  fsm: process(clk, rst)
    begin
      if rst='1' then
        cur_state <= s0;
      elsif clk'event and clk='1'
        cur_state <= nex_state;
    end if;
  end process;

  -- 2e process: logique
  moore: process(s, cur_state)
  begin
    case cur_state is
      when s0 =>
        if s='1' then
          nex_state <= s1;
        else
          -- on peut ne pas l'écrire: mémorisation implicite
          nex_state <= s0;
        end if;
      when s1 =>
        nex_state <= s2;
      when s2 =>
        if s='0' then
          nex_state <= s3;
        else
          nex_state <= s2;
        end if;
      when s3 =>
        nex_state <= s0;
    end case;
  end process;

  -- 3e process: sorties
  output: process(cur_state)
    case cur_state is
      when s1 =>
        sds <= 1;
        sfs <= 0;
      when s3 =>
        sds <= 0;
        sfs <= 1;
      when others =>
        sds <= 0;
        sfs <= 0;
    end case;
  end process;
  
end architecture;
