architecture arch_mealy of mealy is
  type states is (s0, s1);
  signal cur_state, nex_state: states;
begin

  -- 1er process: meme que tout à l'heure
  fsm: process(rst, clk)
  end process;

  -- 2e process: 
  mealy: process(cur_state, s)
  begin
    case cur_state is
      when s0 =>
        if s='1' then
          nex_state <= s1;
        else
          nex_state <= s0;
        end if;
      when s1 =>
        if s='0' then
          nex_state <= s0;
        else
          nex_state <= s1;
        end if;
    end case;
  end process;

  -- envoi des sorties
  sds <= '1' when (cur_state = s0 and s = '1') else '0';
  sfs <= '1' when (cur_state = s1 and s = '0') else '0';

end architecture;
