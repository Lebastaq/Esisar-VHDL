entity cell is
  port(
    clk, rst, start: in std_logic;
    pulse: out std_logic );
end cell;

architecture rtl of cell is
  constant SIZE: integer := 8;
  signal q: std_logic_vector(SIZE-1 downto 0);

begin
  proc: process(rst, clk)
  begin
    if rst='1' then
      q <= (others=>'0');
    elsif clk'event and clk='1' then
      q(0) <= start;
      for in in 0 to (SIZE-2) loop
        q(i+1) <= q(i);
      end loop;
    end if;
  end process;

  pulse <= '0' when q=(others=>0)  else '1';
end rtl;
