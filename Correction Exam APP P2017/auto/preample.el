(TeX-add-style-hook
 "preample"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "11pt" "a4paper" "oneside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "latin1" "utf8") ("geometry" "top=3cm" "bottom=3cm" "left=2.5cm" "right=2.5cm" "headsep=10pt" "a4paper") ("hyperref" "pdfborder={0 0 0}") ("babel" "french") ("mathpazo" "sc" "osf") ("eulervm" "euler-digits" "small") ("parskip" "parfill")))
   (TeX-run-style-hooks
    "latex2e"
    "book"
    "bk11"
    "inputenc"
    "upquote"
    "geometry"
    "graphicx"
    "hyperref"
    "amsmath"
    "babel"
    "mathpazo"
    "inconsolata"
    "minitoc"
    "eulervm"
    "centernot"
    "parskip"
    "booktabs"
    "makecell"
    "tabularx"
    "arydshln"
    "titlesec"
    "fancyhdr"
    "lastpage")
   (TeX-add-symbols
    "tightlist"
    "runtitle")))

