(TeX-add-style-hook
 "Correction"
 (lambda ()
   (TeX-run-style-hooks
    "preample"
    "exo1"
    "exo2"
    "tikz"
    "lstlinebgrd"
    "listingsutf8"
    "color")))

