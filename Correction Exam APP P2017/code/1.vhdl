begin
  tap(0)<=X;
  process(clk)
  begin
    if clk'event and clk='1' then
      if rst='1' then
        for i in 1 to 5 loop
          tap(i) <= (others=>'0');
        end loop;
      else
        for i in 0 to 4 loop
          tap(i+1)<=tap(i);
        end loop;
      end if;
    end if;
  end process;

  y<=(tap(0) + tap(5)) * h(0)(8)&h(0)
      +(tap(1) + tap(4)) * h(1)(8)&h(1)
      +(tap(2) + tap(3)) * h(2)(8)&h(2);
  -- on fait un décalage pour faire passer h sur 10 bits
  -- et ainsi en sortie avoir 8 + 10 bits = 18

  
end architecture;
